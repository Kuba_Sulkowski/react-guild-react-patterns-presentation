import React from "react";

export const Counter = ({
  initialCount = 0,
}: {
  // accept the prop with a default value so it's optional
  initialCount?: number;
}) => {
  const [count, setCount] = React.useState(initialCount); // <-- pass it to your state
  const increment = () => setCount((c) => c + 1);
  const reset = () => setCount(initialCount); // <-- pass that initialCount value to the reset function

  return (
    <div>
      <button onClick={increment}>{count}</button>
      <button onClick={reset}>Reset</button>
    </div>
  );
};

// Protects from api misuse
export const Counter2 = ({
  initialCount = 0,
}: {
  // accept the prop with a default value so it's optional
  initialCount?: number;
}) => {
  const { current: initialState } = React.useRef({
    count: initialCount, // <-- Protect initial value from subsequent changes
  });

  const [count, setCount] = React.useState(initialState.count); // <-- pass it to your state
  const increment = () => setCount((c) => c + 1);

  const reset = () => setCount(initialState.count); // <-- pass that initialCount value to the reset function

  return (
    <div>
      <button onClick={increment}>{count}</button>
      <button onClick={reset}>Reset</button>
    </div>
  );
};
