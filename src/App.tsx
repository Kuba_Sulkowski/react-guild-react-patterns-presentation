import "./App.css";

import { Example } from "./07_Use_Previous/Example";

function App() {
  return (
    <div className="App">
      <h1>Vite + React</h1>
      <div className="card">
        <Example />
      </div>
    </div>
  );
}

export default App;
