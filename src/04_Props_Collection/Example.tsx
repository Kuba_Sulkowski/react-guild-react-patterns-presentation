import React from "react";

function useSlider() {
  /* Some logic here */

  const positionProps = {
    /* ...there would be some data */
  };

  const accessibilityProps = {
    /* ...there would be some data */
  };
  return { positionProps, accessibilityProps };
}

export const Example = () => {
  const { accessibilityProps, positionProps } = useSlider();
  return (
    <>
      {/* We as library autor consume props collections */}
      <LibrarySlider
        accessibilityProps={accessibilityProps}
        positionProps={positionProps}
      />
      {/* And allow user for easier substitution */}
      <div style={{ ...positionProps }} {...accessibilityProps}>
        Example
      </div>
    </>
  );
};

const LibrarySlider = ({
  positionProps,
  accessibilityProps,
}: {
  positionProps: any;
  accessibilityProps: any;
}) => (
  <div style={{ ...positionProps }} {...accessibilityProps}>
    Slider
  </div>
);
