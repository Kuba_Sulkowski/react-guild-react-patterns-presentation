Why to use: 
  - Some components always goes together and share the state, `Compound Component` pattern address this scenario
  - No props drilling
  - End user need not to have any knowledge about internal structure of any component
  - Easy import for end user
  - Still end user can get a snpashot of internal state of a component  