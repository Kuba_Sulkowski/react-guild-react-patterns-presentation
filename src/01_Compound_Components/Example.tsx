import { useState } from "react";

import ClickedCount, { ClickedCountState } from "./Data";

export const Example = () => {
  const [count, setCount] = useState(0);
  const handleClap = (clickedCount: ClickedCountState) => {
    setCount(clickedCount.count);
  };
  return (
    <div style={{ width: "100%" }}>
      <ClickedCount onClick={handleClap}>
        <ClickedCount.Icon />
        <ClickedCount.Count />
        <ClickedCount.Total />
      </ClickedCount>
      {!!count && <div>{`You have clapped ${count} times`}</div>}
    </div>
  );
};
