export const Example = ({
  style = {},
  classNames = [],
}: {
  style?: React.CSSProperties;
  classNames?: string[];
}) => {
  return (
    // out styles go first/last
    <div
      style={{
        ...style,
        color: "red",
      }}
    >
      <div
        className={["myClass1", "myClass2", ...classNames].join(" ")} // or lib like `classnames`
      >
        Example
      </div>
    </div>
  );
};
