type AccessibilityProps =
  | {
      label?: string;
    }
  | undefined;

function useSlider() {
  /* Some logic here */

  const accessibilityProps = ({
    label = "slider",
  }: AccessibilityProps = {}) => ({
    /* ...there would be some data and logic */
    "aria-label": label,
  });

  return { accessibilityProps };
}

const LibrarySlider = ({
  accessibilityProps,
}: {
  accessibilityProps: any;
  // can use the default
}) => <div {...accessibilityProps()}>Slider</div>;

export const Example = () => {
  const { accessibilityProps } = useSlider();
  return (
    <>
      <LibrarySlider accessibilityProps={accessibilityProps} />
      {/* And allow user for easier substitution */}
      <div {...accessibilityProps({ label: "my label" })}>
        Example
      </div>
    </>
  );
};
