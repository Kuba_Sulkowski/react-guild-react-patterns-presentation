import React from "react";

const WithOpen = ({
  children,
}: {
  children: ({ open }: { open: boolean }) => JSX.Element;
}) => {
  const [open] = React.useState(false);

  return children({ open });
};

const Child = ({ open, color }: { open: boolean; color: string }) =>
  open ? <div style={{ color }}></div> : <></>;

export const App = () => {
  return (
    <WithOpen>
      {({ open }) => <Child color="red" open={open} />}
    </WithOpen>
  );
};
