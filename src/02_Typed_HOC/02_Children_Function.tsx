import React from "react";

const withOpen = <P extends { open: boolean }>(
  Component: (props: P) => JSX.Element, // Cant use React.ReactNode here, dont know why
) => {
  const [open] = React.useState(false);

  // `as P` is essential, otherwise Typescript complains and finding what is it about is difficult...
  return (props: Omit<P, "open">) => (
    <Component {...(props as P)} open={open} />
  );
};

const Child = ({ open, color }: { open: boolean; color: string }) =>
  open ? <div style={{ color }}></div> : <></>;

const ChildWithOpen = withOpen(Child);

export const App = () => {
  return <ChildWithOpen color={"red"} />;
};
