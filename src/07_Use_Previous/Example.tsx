import React from "react";

function usePrevious<T>(val: T, initial?: T) {
  const ref = React.useRef<T>();

  React.useEffect(() => {
    ref.current = val;
    // fires second
    console.log(ref.current);
  }, [val]);

  // fires first
  console.log(ref.current);

  // Return previous value (happens before update in useEffect above)
  return ref.current ?? initial;
}

export const Example = () => {
  const [count, setCount] = React.useState(0);
  const previousCount = usePrevious(count, 0);
  return (
    <div>
      <p>
        {"Previous: "} {previousCount}{" "}
      </p>
      <p>
        {"Current: "} {count}{" "}
      </p>
      <button onClick={() => setCount((prev) => prev + 1)}>
        Increment
      </button>
    </div>
  );
};
